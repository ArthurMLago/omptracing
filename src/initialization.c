#include "initialization.h"
#include "debug.h"
#define _GNU_SOURCE
#include <dlfcn.h>

/** \var static int error
    \brief Indicate if error ocurred or not in json file write, your values are
   defined by constants ERROR_OCCURRED and ERROR_NOT_OCCUR.
*/
static int error;

static ompt_get_thread_data_t ompt_get_thread_data;
static uint64_t ID_TASK = 0, ID_PARALLEL = 0, ID_WORK = 0, ID_IMPLICIT = 0,
                ID_SYNC = 0, ID_MASTER = 0, ID_DEPEND_IN = 0, ID_DEPEND_OUT = 0;

/*!
 * Allocate new task data pointer.
 * @return Task data pointer.
 */
task_data *task_data_pointer() {
  int i = 0;
  task_data *pointer = (task_data *)malloc(sizeof(task_data));
  pointer->level = 0;
  pointer->parent_id = -1;
  pointer->time = -1;

  for (i = 0; i < NUM_IDS; i++)
    pointer->id[i] = -1;
  return pointer;
}

/*!
 * Get time in ms.
 * @return The current time in ms.
 */
double get_time() {
  struct timeval time_addr;
  gettimeofday(&time_addr, NULL);
  double time =
      (time_addr.tv_sec * (uint64_t)1000) + (time_addr.tv_usec / 1000);
  return time - initial_time;
}

/*!
 * Increment the param id.
 * @param ID is the pointer to id that will be incremented.
 * @return The next id value.
 */
static uint64_t my_next_id(uint64_t *ID) {
  uint64_t ret = __sync_fetch_and_add(ID, 1);
  return ret;
}

uint64_t *id_pointer(uint64_t *ID) {
  uint64_t *pointer = (uint64_t *)malloc(sizeof(uint64_t));
  (*pointer) = my_next_id(ID);
  return pointer;
}

/*!
 * Convert long integer to string..
 * @param num is the long int that will be converted.
 * @return The number in string format.
 */
char *str_id(long int num) {
  char *str = (char *)malloc(20 * sizeof(char));
  sprintf(str, "%lu", num);
  return str;
}

/*!
 * Concatenate string and long int.
 * @param name is the string that will be concatenated.
 * @param num is the long integer that will be concatenated.
 * @return The string namenum.
 */
char *conc_str_int(char *name, long int num) {
  char *str = (char *)malloc(20 * sizeof(char));
  sprintf(str, "%s%ld", name, num);
  return str;
}

/*!
 * Callback to get information of parallel region begin.
 * @param encountering_task_data is the encountering task.
 * @param encountering_task_frame points to the frame object associated with the
 * encountering task.
 * @param parallel_data is the parallel or teams region that is beginning.
 * @param requested_parallelism indicates the number of threads or teams
 * requested by the user.
 * @param flags indicates whether the code for the parallel region is inlined
 * into the application or invoked by the runtime and also whether the region is
 * a parallel or teams region.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void on_ompt_callback_parallel_begin(
    ompt_data_t *encountering_task_data,
    const ompt_frame_t *encountering_task_frame, ompt_data_t *parallel_data,
    unsigned int requested_parallelism, int flags, const void *codeptr_ra) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  arg *args = NULL;
  char *str;

  // Add ompt information.
  str = conc_str_int("", requested_parallelism);
  args = add_args(args, "Requested parallelism:", str, &tk_list);

  // Get the id of parallel region.
  parallel_data->ptr = id_pointer(&ID_PARALLEL);
  str = conc_str_int("Parallel Region ", *((int *)parallel_data->ptr));

  // Add task begin.
  add_task("OMPT", -1, "Parallel Region", str, 'B', get_time(), args, &tk_list);
}

/*!
 * Callback to get information of parallel region end.
 * @param parallel_data is the parallel or teams region that is ending.
 * @param encountering_task_data is the encountering task.
 * @param flags indicates whether the execution of the parallel region is
 * inlined into the application or invoked by the runtime and also whether the
 * region is a parallel or teams region.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void on_ompt_callback_parallel_end(ompt_data_t *parallel_data,
                                          ompt_data_t *encountering_task_data,
                                          int flags, const void *codeptr_ra) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  // Add task end.
  char *str = conc_str_int("Parallel Region ", *((int *)parallel_data->ptr));
  add_task("OMPT", -1, "Parallel Region", str, 'E', get_time(), NULL, &tk_list);
}

/*!
 * Callback to get information of thread region begin.
 * @param thread_type indicates the type of the new thread: initial, worker, or
 * other.
 * @param thread_data is the new thread.
 */
static void on_ompt_callback_thread_begin(ompt_thread_t thread_type,
                                          ompt_data_t *thread_data) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  arg *args = NULL;
  char *str;

  // Get thread type.
  switch (thread_type) {
  case 1:
    str = "Initial";
    break;
  case 2:
    str = "Worker";
    break;
  case 3:
    str = "Other";
    break;
  default:
    str = "Unknown";
    break;
  }

  // Add task info.
  args = add_args(args, "Type:", str, &tk_list);

  // Add task begin.
  add_task("OMPT", omp_get_thread_num(), "Thread Region", "Thread Region", 'B',
           get_time(), args, &tk_list);
}

/*!
 * Callback to get information of thread region end.
 * @param thread_data is the thread that is terminating.
 */
static void on_ompt_callback_thread_end(ompt_data_t *thread_data) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  // Add task end.
  add_task("OMPT", omp_get_thread_num(), "Thread Region", "Thread Region", 'E',
           get_time(), NULL, &tk_list);
}

/*!
 * Callback to get information of the created task.
 * @param encountering_task_data is the encountering task. This parameter is
 * NULL for an initial task.
 * @param encountering_task_frame  points to the frame object associated with
 * the encountering task. This parameter is NULL for an initial task.
 * @param new_task_data is the created task.
 * @param flags indicates the kind of the task: initial, explicit or target.
 * Values for flag are composed by or-ing elements of enum ompt_task_flag_t.
 * @param has_dependences indicates whether created task has dependences.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void
on_ompt_callback_task_create(ompt_data_t *encountering_task_data,
                             const ompt_frame_t *encountering_task_frame,
                             ompt_data_t *new_task_data, int flags,
                             int has_dependences, const void *codeptr_ra) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  char *str;
  arg *args = NULL;

  // Add task information to ompt data.
  if (new_task_data->ptr == NULL) {
    new_task_data->ptr = task_data_pointer();
    ((task_data *)new_task_data->ptr)->time = get_time();
  } else
    args = ((task_data *)new_task_data->ptr)->args;

  // Add task id.
  if (((task_data *)new_task_data->ptr)->id[0] == -1)
    ((task_data *)new_task_data->ptr)->id[0] = my_next_id(&ID_TASK);

  if (dp_list.max_task_id < ((task_data *)new_task_data->ptr)->id[0] &&
      ((task_data *)new_task_data->ptr)->id[0] < dp_list.dependence_size)
    dp_list.max_task_id = ((task_data *)new_task_data->ptr)->id[0];

  // Add info of parent task if exists.
  if (encountering_task_data != NULL) {
    if (((task_data *)encountering_task_data->ptr) != NULL) {
      // Add info of parent id.
      if (((task_data *)encountering_task_data->ptr)->id[0] == -1) {
        if (((task_data *)encountering_task_data->ptr)->id[1] != -1) {
          str = conc_str_int("",
                             ((task_data *)encountering_task_data->ptr)->id[1]);
          args = add_args(args, "Work parent:", str, &tk_list);
        } else if (((task_data *)encountering_task_data->ptr)->id[2] != -1) {
          str = conc_str_int("",
                             ((task_data *)encountering_task_data->ptr)->id[2]);
          args = add_args(args, "Implicit task parent:", str, &tk_list);
        } else if (((task_data *)encountering_task_data->ptr)->id[3] != -1) {
          str = conc_str_int("",
                             ((task_data *)encountering_task_data->ptr)->id[3]);
          args = add_args(args, "Master parent:", str, &tk_list);
        } else if (((task_data *)encountering_task_data->ptr)->id[4] != -1) {
          str = conc_str_int("",
                             ((task_data *)encountering_task_data->ptr)->id[4]);
          args = add_args(args, "Sync parent:", str, &tk_list);
        }
      } else
        ((task_data *)new_task_data->ptr)->parent_id =
            ((task_data *)encountering_task_data->ptr)->id[0];
      // Increment task level.
      ((task_data *)new_task_data->ptr)->level =
          ((task_data *)encountering_task_data->ptr)->level + 1;
    }
  }

  // Get task type.
  if (flags & ompt_task_explicit)
    str = "Explicit";
  else if (flags & ompt_task_initial)
    str = "Initial";
  else if (flags & ompt_task_target)
    str = "Target";
  else if (flags & ompt_task_undeferred)
    str = "Undeferred";
  else if (flags & ompt_task_untied)
    str = "Untied";
  else if (flags & ompt_task_final)
    str = "Final";
  else if (flags & ompt_task_mergeable)
    str = "Mergeable";
  else if (flags & ompt_task_merged)
    str = "Merged";

  // Add info in arg list.
  Dl_info dli;
  dli.dli_sname = NULL;
  args = add_args(args, "Type:", str, &tk_list);
  if (codeptr_ra != NULL){
    Dl_info dli;
    dladdr(codeptr_ra, &dli);
    if (dli.dli_sname != NULL){
      char *debug_symbol_string = (char*)malloc(128);
      sprintf(debug_symbol_string, "%s", dli.dli_sname);
      args = add_args(args, "sname", debug_symbol_string, &tk_list);
    }
  }
  char * = (char*)malloc(20);
  sprintf(debug_symbol_string2, "0x%lX", codeptr_ra);
  args = add_args(args, "codeptr_ra", debug_symbol_string2, &tk_list);
  ((task_data *)new_task_data->ptr)->args = args;

  // Add task create
  if (dli.dli_sname != NULL){
    str = malloc(128);
    sprintf(str, "id_%lu (%s)", ((task_data *)new_task_data->ptr)->id[0], dli.dli_sname);
  }else{
    str = conc_str_int("id_", ((task_data *)new_task_data->ptr)->id[0]);
  }

  if (task_create == CONFIG_SELECTED)
    add_task("OMPT", omp_get_thread_num(), "Task Create", str, 'I',
             ((task_data *)new_task_data->ptr)->time, args, &tk_list);
}

/*!
 * Callback to get information of the task schedule.
 * @param prior_task_data indicates the status of the task that arrived at a
 * task scheduling point.
 * @param prior_task_status is the task that arrived at the scheduling point.
 * @param next_task_data is the task that will resume at the scheduling point.
 */
static void on_ompt_callback_task_schedule(ompt_data_t *prior_task_data,
                                           ompt_task_status_t prior_task_status,
                                           ompt_data_t *next_task_data) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  uint64_t tid = ompt_get_thread_data()->value;

  // Verify if ompt data exist.
  if (prior_task_data != NULL) {
    // Task complete.
    if (prior_task_data->ptr != NULL &&
        prior_task_status == ompt_task_complete) {
      char *str, *str_n;

      arg *args = NULL;
      // Get parent info.
      if (((task_data *)prior_task_data->ptr)->parent_id != -1) {
        str = conc_str_int("", ((task_data *)prior_task_data->ptr)->parent_id);
        args = add_args(args, "Parent:", str, &tk_list);
      }

      // Add task begin and task end.
      // Retrieve symbol name if available
      arg *created_task_args = ((task_data *)prior_task_data->ptr)->args;
      char *sname_arg = get_arg(created_task_args, "sname");
      if (sname_arg != NULL){
        str = (char*)malloc(128);
        sprintf(str, "id_%lu (%s)", ((task_data *)prior_task_data->ptr)->id[0], sname_arg);
      }else{
        str = conc_str_int("id_", ((task_data *)prior_task_data->ptr)->id[0]);
      }
      str_n = conc_str_int("Tasks Level ",
                           ((task_data *)prior_task_data->ptr)->level);
      add_task("OMPT", omp_get_thread_num(), str_n, str, 'B',
               ((task_data *)prior_task_data->ptr)->time,
               created_task_args, &tk_list);
      double finish_time = get_time();
      double elapsed_time =
          finish_time - ((task_data *)prior_task_data->ptr)->time;

      add_task("OMPT", omp_get_thread_num(), str_n, str, 'E', finish_time, args,
               &tk_list);

      if (dp_list.graph_time == GRAPH_TIME_SELECTED) {
        add_time_task(((task_data *)prior_task_data->ptr)->id[0],
                      ((task_data *)prior_task_data->ptr)->time, finish_time,
                      &tk_time_list, NOT_TASKWAIT_TYPE);
      }

      if (dp_list.critical_path == CRITICAL_PATH_SELECTED) {
        add_elapsed_time(((task_data *)prior_task_data->ptr)->id[0],
                         elapsed_time, &dp_list);
      }

    } else if (next_task_data != NULL) {
      if (next_task_data->ptr != NULL &&
          prior_task_status == ompt_task_switch) {
        ((task_data *)next_task_data->ptr)->time = get_time();

        // Add begin time to taskwait
        if (tk_list.taskwait == TASKWAIT_SELECTED) {
          if (((task_data *)next_task_data->ptr)->id[0] <
              dp_list.dependence_size) {
            tasks_begin_time[((task_data *)next_task_data->ptr)->id[0]] =
                ((task_data *)next_task_data->ptr)->time;
          } else {
            printf("The number of tasks is greater than the selected "
                   "max_tasks. Some "
                   "tasks will not be recorded in the graph.\n");
          }
        }
      }
    }
  }
}

/*!
 * Get string with type of work region.
 * @param wstype is the type of the work region.
 * @param new_task_data is the associate task data of the work region.
 * @return The name of work region in string format.
 */
char *get_work_name(ompt_work_t wstype, ompt_data_t *new_task_data) {
  char *str;

  if (wstype & ompt_work_loop) {
    str = conc_str_int("loop_", ((task_data *)new_task_data->ptr)->id[1]);
  } else if (wstype & ompt_work_sections) {
    str = conc_str_int("sections_", ((task_data *)new_task_data->ptr)->id[1]);
  } else if (wstype & ompt_work_workshare) {
    str = conc_str_int("workshare_", ((task_data *)new_task_data->ptr)->id[1]);
  } else if (wstype & ompt_work_single_executor) {
    str = conc_str_int("single_executor_",
                       ((task_data *)new_task_data->ptr)->id[1]);
  } else if (wstype & ompt_work_single_other) {
    str =
        conc_str_int("single_other_", ((task_data *)new_task_data->ptr)->id[1]);
  } else if (wstype & ompt_work_distribute) {
    str = conc_str_int("distribute_", ((task_data *)new_task_data->ptr)->id[1]);
  } else if (wstype & ompt_work_taskloop) {
    str = conc_str_int("taskloop_", ((task_data *)new_task_data->ptr)->id[1]);
  }

  return str;
}

/*!
 * Callback to get information of the work region begin/end.
 * @param wstype indicates the kind of worksharing region.
 * @param endpoint indicates whether the callback is signalling the beginning or
 * the end of a scope.
 * @param parallel_data is the current parallel region.
 * @param new_task_data is the current task.
 * @param count is a measure of the quantity of work involved in the worksharing
 * construct.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void on_ompt_callback_work(ompt_work_t wstype,
                                  ompt_scope_endpoint_t endpoint,
                                  ompt_data_t *parallel_data,
                                  ompt_data_t *new_task_data, uint64_t count,
                                  const void *codeptr_ra) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  if (endpoint == 1) {
    // Begin.
    char *str = conc_str_int("", count);
    arg *args = NULL;

    // Add ompt data.
    if (new_task_data->ptr == NULL) {
      new_task_data->ptr = task_data_pointer();
      ((task_data *)new_task_data->ptr)->time = get_time();
    } else
      args = ((task_data *)new_task_data->ptr)->args;

    // Add work id.
    if (((task_data *)new_task_data->ptr)->id[1] == -1)
      ((task_data *)new_task_data->ptr)->id[1] = my_next_id(&ID_WORK);

    // Add ompt work info.
    if (wstype & ompt_work_loop) {
      args = add_args(args, "Number of iterations: ", str, &tk_list);
    } else if (wstype & ompt_work_sections) {
      args = add_args(args, "Number of sections: ", str, &tk_list);
    } else if (wstype & ompt_work_workshare) {
      args = add_args(args, "Units or work: ", str, &tk_list);
    } else if (wstype & ompt_work_taskloop) {
      args = add_args(args, "Number of iterations: ", str, &tk_list);
    }
    ((task_data *)new_task_data->ptr)->args = args;

    // Add task work begin.
    str = get_work_name(wstype, new_task_data);
    add_task("OMPT", omp_get_thread_num(), "Work Region", str, 'B', get_time(),
             args, &tk_list);
  } else if (new_task_data != NULL) {
    // End.
    if (new_task_data->ptr != NULL) {
      char *str = get_work_name(wstype, new_task_data);

      // Add task work end.
      arg *args = ((task_data *)new_task_data->ptr)->args;
      add_task("OMPT", omp_get_thread_num(), "Work Region", str, 'E',
               get_time(), args, &tk_list);
    }
  }
}

static void on_ompt_callback_dependences(ompt_data_t *tk_data,
                                         const ompt_dependence_t *deps,
                                         int ndeps) {
  char *str, *str_n;
  arg *args = NULL;

  // Add task ompt data.
  if (tk_data->ptr == NULL) {
    tk_data->ptr = task_data_pointer();
    ((task_data *)tk_data->ptr)->time = get_time();
  } else
    args = ((task_data *)tk_data->ptr)->args;

  // Add task id.
  if (((task_data *)tk_data->ptr)->id[0] == -1)
    ((task_data *)tk_data->ptr)->id[0] = my_next_id(&ID_TASK);

  if (dp_list.max_task_id < ((task_data *)tk_data->ptr)->id[0] &&
      ((task_data *)tk_data->ptr)->id[0] < dp_list.dependence_size)
    dp_list.max_task_id = ((task_data *)tk_data->ptr)->id[0];

  // Check all dependences
  for (int i = 0; i < ndeps; i++) {

    // Convert task id to string
    str = conc_str_int("", (((ompt_dependence_t)(*(deps + i))).variable).value);

    // Search out dependence of in dependence
    if ((((ompt_dependence_t)(*(deps + i))).dependence_type ==
         ompt_dependence_type_in) ||
        /*(((ompt_dependence_t)(*(deps + i))).dependence_type ==
         ompt_dependence_type_inout) ||*/
        (((ompt_dependence_t)(*(deps + i))).dependence_type ==
         ompt_dependence_type_source)) {
      int dep = search_out_dep(
          tk_data, ((task_data *)tk_data->ptr)->id[0],
          (((ompt_dependence_t)(*(deps + i))).variable).value, &out, &dp_list);
      if (dep == 0) {
        add_task_data(tk_data, ((task_data *)tk_data->ptr)->id[0],
                      (((ompt_dependence_t)(*(deps + i))).variable).value, &in);
      }
      str_n = conc_str_int("Depend in_", my_next_id(&ID_DEPEND_IN));
      args = add_args(args, str_n, str, &tk_list);
      ((task_data *)tk_data->ptr)->args = args;
    }

    // Add out dependence
    if ((((ompt_dependence_t)(*(deps + i))).dependence_type ==
         ompt_dependence_type_out) ||
        (((ompt_dependence_t)(*(deps + i))).dependence_type ==
         ompt_dependence_type_inout) ||
        (((ompt_dependence_t)(*(deps + i))).dependence_type ==
         ompt_dependence_type_sink)) {
      add_task_data(tk_data, ((task_data *)tk_data->ptr)->id[0],
                    (((ompt_dependence_t)(*(deps + i))).variable).value, &out);
      // Add in -> out deps
      int dep = search_in_dep(
          tk_data, ((task_data *)tk_data->ptr)->id[0],
          (((ompt_dependence_t)(*(deps + i))).variable).value, &in, &dp_list);
      // Add out -> out deps
      if (dep == 0) {
        search_out_dep(tk_data, ((task_data *)tk_data->ptr)->id[0],
                       (((ompt_dependence_t)(*(deps + i))).variable).value,
                       &out, &dp_list);
      }
      str_n = conc_str_int("Depend out_", my_next_id(&ID_DEPEND_OUT));
      args = add_args(args, str_n, str, &tk_list);
      ((task_data *)tk_data->ptr)->args = args;
    }
  }
}

/*!
 * Callback to get information of the task dependence.
 * @param src_task_data is a running task with an outgoing dependence.
 * @param sink_task_data is a task with an unsatisfied incoming dependence.
 */
static void on_ompt_callback_task_dependence(ompt_data_t *src_task_data,
                                             ompt_data_t *sink_task_data) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  char *str, *str_n;
  arg *args_sink = NULL, *args_src = NULL;

  // Add source ompt data.
  if (src_task_data->ptr == NULL) {
    src_task_data->ptr = task_data_pointer();
    ((task_data *)src_task_data->ptr)->time = get_time();
  } else
    args_src = ((task_data *)src_task_data->ptr)->args;

  // Add source task id.
  if (((task_data *)src_task_data->ptr)->id[0] == -1)
    ((task_data *)src_task_data->ptr)->id[0] = my_next_id(&ID_TASK);

  if (dp_list.max_task_id < ((task_data *)src_task_data->ptr)->id[0] &&
      ((task_data *)src_task_data->ptr)->id[0] < dp_list.dependence_size)
    dp_list.max_task_id = ((task_data *)src_task_data->ptr)->id[0];

  // Add sink ompt data.
  if (sink_task_data->ptr == NULL) {
    sink_task_data->ptr = task_data_pointer();
    ((task_data *)sink_task_data->ptr)->time = get_time();
  } else
    args_sink = ((task_data *)sink_task_data->ptr)->args;

  // Add sink task id.
  if (((task_data *)sink_task_data->ptr)->id[0] == -1)
    ((task_data *)sink_task_data->ptr)->id[0] = my_next_id(&ID_TASK);

  if (dp_list.max_task_id < ((task_data *)sink_task_data->ptr)->id[0] &&
      ((task_data *)sink_task_data->ptr)->id[0] < dp_list.dependence_size)
    dp_list.max_task_id = ((task_data *)sink_task_data->ptr)->id[0];

  // Add graph dependence.
  add_dependence(((task_data *)sink_task_data->ptr)->id[0],
                 ((task_data *)src_task_data->ptr)->id[0], &dp_list);

  // Add task depend in.
  str = conc_str_int("", ((task_data *)src_task_data->ptr)->id[0]);
  str_n = conc_str_int("Depend in_", my_next_id(&ID_DEPEND_IN));
  args_sink = add_args(args_sink, str_n, str, &tk_list);
  ((task_data *)sink_task_data->ptr)->args = args_sink;

  // Add task depend out.
  str = conc_str_int("", ((task_data *)sink_task_data->ptr)->id[0]);
  str_n = conc_str_int("Depend out_", my_next_id(&ID_DEPEND_OUT));
  args_src = add_args(args_src, str_n, str, &tk_list);
  ((task_data *)src_task_data->ptr)->args = args_src;
}

/*!
 * Callback to get information of the implicit region begin/end.
 * @param endpoint indicates whether the callback is signalling the beginning or
 * the end of a scope.
 * @param parallel_data is the current parallel region. For the
 * implicit-task-end event, this argument is NULL.
 * @param new_task_data is the implicit task executing the parallel region’s
 * structured block.
 * @param actual_parallelism indicates the number of threads in the parallel
 * region, respectively the number of teams in the teams region. For the
 * implicit-task-end and the initial-task-end events, this argument is 0.
 * @param index indicates the thread number or team number of the calling
 * thread, within the team or league executing the parallel or teams region to
 * which the implicit region binds.
 * @param flags.
 */
static void on_ompt_callback_implicit_task(ompt_scope_endpoint_t endpoint,
                                           ompt_data_t *parallel_data,
                                           ompt_data_t *new_task_data,
                                           unsigned int actual_parallelism,
                                           unsigned int index, int flags) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  if (endpoint == 1) {
    // Begin.
    char *str;
    arg *args = NULL;

    // Add ompt data.
    if (new_task_data->ptr == NULL) {
      new_task_data->ptr = task_data_pointer();
    } else
      args = ((task_data *)new_task_data->ptr)->args;

    // Add implicit id.
    if (((task_data *)new_task_data->ptr)->id[2] == -1)
      ((task_data *)new_task_data->ptr)->id[2] = my_next_id(&ID_IMPLICIT);

    // Add ompt info.
    str = conc_str_int("", actual_parallelism);
    args = add_args(args, "Number of threads/teams:", str, &tk_list);
    str = conc_str_int("", index);
    args = add_args(args, "Thread/Team number:", str, &tk_list);
    ((task_data *)new_task_data->ptr)->args = args;

    // Add implicit task begin.
    str = conc_str_int("implicit_", ((task_data *)new_task_data->ptr)->id[2]);
    add_task("OMPT", omp_get_thread_num(), "Tasks Level 0", str, 'B',
             get_time(), ((task_data *)new_task_data->ptr)->args, &tk_list);
  } else if (new_task_data != NULL) {
    // End.
    if (new_task_data->ptr != NULL) {
      char *str =
          conc_str_int("implicit_", ((task_data *)new_task_data->ptr)->id[2]);
      arg *args = NULL;

      // Add implicit task end.
      add_task("OMPT", omp_get_thread_num(), "Tasks Level 0", str, 'E',
               get_time(), ((task_data *)new_task_data->ptr)->args, &tk_list);
    }
  }
}

/*!
 * Callback to get information of the master region begin/end.
 * @param endpoint indicates whether the callback is signalling the beginning or
 * the end of a scope.
 * @param parallel_data is the current parallel region.
 * @param new_task_data is the encountering task.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void on_ompt_callback_master(ompt_scope_endpoint_t endpoint,
                                    ompt_data_t *parallel_data,
                                    ompt_data_t *new_task_data,
                                    const void *codeptr_ra) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  if (endpoint == 1) {
    // Begin.

    // Add ompt data.
    if (new_task_data->ptr == NULL) {
      new_task_data->ptr = task_data_pointer();
      ((task_data *)new_task_data->ptr)->args = NULL;
    }

    // Add master id.
    if (((task_data *)new_task_data->ptr)->id[3] == -1)
      ((task_data *)new_task_data->ptr)->id[3] = my_next_id(&ID_MASTER);

    // Add master task begin.
    char *str =
        conc_str_int("master_", ((task_data *)new_task_data->ptr)->id[3]);
    add_task("OMPT", omp_get_thread_num(), "Master Task", str, 'B', get_time(),
             ((task_data *)new_task_data->ptr)->args, &tk_list);

  } else if (new_task_data != NULL) {
    // End.
    if (new_task_data->ptr != NULL) {
      char *str =
          conc_str_int("master_", ((task_data *)new_task_data->ptr)->id[3]);

      // Add master task end.
      add_task("OMPT", omp_get_thread_num(), "Master Task", str, 'E',
               get_time(), ((task_data *)new_task_data->ptr)->args, &tk_list);
    }
  }
}

/*!
 * Get type of the sync region.
 * @param kind is the type of sync region.
 * @param new_task_data is the associate task data of the sync region.
 * @return The type of sync region in string format.
 */
char *get_sync_name(ompt_sync_region_t kind, ompt_data_t *new_task_data) {
  char *str;

  if (kind & ompt_sync_region_barrier)
    str = conc_str_int("region_barrier_",
                       ((task_data *)new_task_data->ptr)->id[4]);
  else if (kind & ompt_sync_region_barrier_implicit)
    str = conc_str_int("barrier_implicit_",
                       ((task_data *)new_task_data->ptr)->id[4]);
  else if (kind & ompt_sync_region_barrier_explicit)
    str = conc_str_int("barrier_explicit_",
                       ((task_data *)new_task_data->ptr)->id[4]);
  else if (kind & ompt_sync_region_barrier_implementation)
    str = conc_str_int("barrier_implementation_",
                       ((task_data *)new_task_data->ptr)->id[4]);
  else if (kind & ompt_sync_region_taskwait)
    str = conc_str_int("taskwait_", ((task_data *)new_task_data->ptr)->id[4]);
  else if (kind & ompt_sync_region_taskgroup)
    str = conc_str_int("taskgroup_", ((task_data *)new_task_data->ptr)->id[4]);
  else if (kind & ompt_sync_region_reduction)
    str = conc_str_int("reduction_", ((task_data *)new_task_data->ptr)->id[4]);

  return str;
}

/*!
 * Callback to get information of the sync region begin/end.
 * @param kind indicates the kind of synchronization.
 * @param endpoint indicates whether the callback is signalling the beginning or
 * the end of a scope.
 * @param parallel_data is the current parallel region. For the barrier-end
 * event at the end of a parallel region, this argument is NULL..
 * @param new_task_data is the current task.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void on_ompt_callback_sync_region(ompt_sync_region_t kind,
                                         ompt_scope_endpoint_t endpoint,
                                         ompt_data_t *parallel_data,
                                         ompt_data_t *new_task_data,
                                         const void *codeptr_ra) {
  // Verify error in json file write.
  if (error == ERROR_OCCURRED)
    return;

  if (endpoint == 1) {
    // Begin.

    // Add ompt data.
    if (new_task_data->ptr == NULL) {
      new_task_data->ptr = task_data_pointer();
      ((task_data *)new_task_data->ptr)->args = NULL;
    }

    if (((task_data *)new_task_data->ptr)->time == -1)
      ((task_data *)new_task_data->ptr)->time = get_time();

    // Add sync id.
    if (((task_data *)new_task_data->ptr)->id[4] == -1)
      ((task_data *)new_task_data->ptr)->id[4] = my_next_id(&ID_SYNC);

    // Add sync task begin.
    char *str = get_sync_name(kind, new_task_data);
    add_task("OMPT", omp_get_thread_num(), "Sync Region", str, 'B',
             ((task_data *)new_task_data->ptr)->time,
             ((task_data *)new_task_data->ptr)->args, &tk_list);

    // Add taskwait task.
    if ((kind & ompt_sync_region_taskwait) &&
        tk_list.taskwait == TASKWAIT_SELECTED) {
      // Add task id.
      if (((task_data *)new_task_data->ptr)->id[0] == -1)
        ((task_data *)new_task_data->ptr)->id[0] = my_next_id(&ID_TASK);

      if (dp_list.max_task_id < ((task_data *)new_task_data->ptr)->id[0] &&
          ((task_data *)new_task_data->ptr)->id[0] < dp_list.dependence_size)
        dp_list.max_task_id = ((task_data *)new_task_data->ptr)->id[0];

      if (taskwait_exists(((task_data *)new_task_data->ptr)->id[0],
                          &tkwait_list) == 0) {
        // Add task in taskwait list.
        add_task_data(new_task_data, ((task_data *)new_task_data->ptr)->id[0],
                      0, &tkwait_list);
        // Add taskwait time
        if (((task_data *)new_task_data->ptr)->id[0] <
            dp_list.dependence_size) {
          tasks_begin_time[((task_data *)new_task_data->ptr)->id[0]] =
              ((task_data *)new_task_data->ptr)->time;
        } else {
          printf("The number of tasks is greater than the selected max_tasks. "
                 "Some tasks will not be recorded in the graph.\n");
        }
      }
    }
  } else if (new_task_data != NULL) {
    // End.
    if (new_task_data->ptr != NULL) {
      char *str = get_sync_name(kind, new_task_data);

      // Add sync task end.
      double finish_time = get_time();
      add_task("OMPT", omp_get_thread_num(), "Sync Region", str, 'E',
               finish_time, ((task_data *)new_task_data->ptr)->args, &tk_list);

      // Add taskwait task time.
      if ((kind & ompt_sync_region_taskwait) &&
          dp_list.graph_time == GRAPH_TIME_SELECTED &&
          tk_list.taskwait == TASKWAIT_SELECTED) {
        add_time_task(((task_data *)new_task_data->ptr)->id[0],
                      ((task_data *)new_task_data->ptr)->time, finish_time,
                      &tk_time_list, TASKWAIT_TYPE);
      }
    }
  }
}

/*!
 * Callback required for the target callbacks to work. Although it's never
 * called.
 * @param device_num identifies the logical device being initialized.
 * @param type indicates the type of the device.
 * @param device is a pointer to an opaque object that represents the target
 * device instance. It's used by functions in the device tracing interface to
 * identify the device being addressed.
 * @param lookup is a pointer to a runtime callback that a tool must use to
 * obtain pointers to runtime entry points in the device’s OMPT tracing
 * interface.
 * @param documentation is a string that describes how to use any
 * device-specific runtime entry points that can be obtained using lookup.
 */
static void on_ompt_callback_device_initialize(int device_num, const char *type,
                                               ompt_device_t *device,
                                               ompt_function_lookup_t lookup,
                                               const char *documentation) {
  // This callback is necessary for the target callbacks to work.
}

/*!
 * Callback to get information about initial target task created on a target
 * device.
 * @param endpoint indicates whether the callback is signalling the beginning or
 * the end of a scope.
 * @param target_id is a unique identifier for the associated target region.
 * @param host_op_id is a unique identifer for the initial task on the target
 * device.
 * @param requested_num_teams is the number of teams that the host is requesting
 * to execute the kernel.
 */
static void on_ompt_callback_target_submit(ompt_scope_endpoint_t endpoint,
                                           ompt_id_t target_id,
                                           ompt_id_t host_op_id,
                                           unsigned int requested_num_teams) {
  // Concat id with task name.
  char *str = conc_str_int("target_submit_", target_id);

  if (endpoint == ompt_scope_begin) {
    // Begin.
    arg *args = NULL;
    char *str_arg;

    // Add ompt information.
    str_arg = conc_str_int("", requested_num_teams);
    args = add_args(args, "Requested num of teams:", str_arg, &tk_list);
    str_arg = conc_str_int("", host_op_id);
    args = add_args(args, "Host id:", str_arg, &tk_list);

    // Add target task begin.
    add_task("OMPT", omp_get_thread_num(), "Target Submit", str, 'B',
             get_time(), args, &tk_list);

  } else {
    // End.

    // Add target task end.
    add_task("OMPT", omp_get_thread_num(), "Target Submit", str, 'E',
             get_time(), NULL, &tk_list);
  }
}

/*!
 * Callback to get information about data copied to or from a device.
 * @param endpoint indicates whether the callback is signalling the beginning or
 * the end of a scope.
 * @param target_id is a unique identifier for the associated target region.
 * @param host_op_id is a unique identifer for the initial task on the target
 * device.
 * @param optype indicates the kind of data mapping.
 * @param src_addr indicates the address of data before the operation, where
 * applicable.
 * @param src_device_num indicates the source device number for the data
 * operation, where applicable.
 * @param dest_addr indicates the address of data after the operation.
 * @param dest_device_num indicates the destination device number for the data
 * operation.
 * @param bytes indicates the size of data.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void on_ompt_callback_target_data_op(
    ompt_scope_endpoint_t endpoint, ompt_id_t target_id, ompt_id_t host_op_id,
    ompt_target_data_op_t optype, void *src_addr, int src_device_num,
    void *dest_addr, int dest_device_num, size_t bytes,
    const void *codeptr_ra) {
  // Concat id with task name.
  char *str = conc_str_int("target_data_", target_id);

  if (endpoint == ompt_scope_begin) {
    // Begin.
    arg *args = NULL;
    char *str_arg;

    // Add ompt information.
    str_arg = conc_str_int("", host_op_id);
    args = add_args(args, "Host id:", str_arg, &tk_list);
    str_arg = conc_str_int("", bytes);
    args = add_args(args, "Size of data:", str_arg, &tk_list);

    switch (optype) {
    case ompt_target_data_alloc:
      str_arg = "Alloc.";
      break;
    case ompt_target_data_transfer_to_device:
      str_arg = "Transfer to device.";
      break;
    case ompt_target_data_transfer_from_device:
      str_arg = "Transfer from device.";
      break;
    case ompt_target_data_delete:
      str_arg = "Delete.";
      break;
    case ompt_target_data_associate:
      str_arg = "Associate.";
      break;
    case ompt_target_data_disassociate:
      str_arg = "Disassociate.";
      break;
    default:
      str_arg = "Unknown.";
      break;
    }
    args = add_args(args, "Kind of data mapping:", str_arg, &tk_list);

    // Add target task begin.
    add_task("OMPT", omp_get_thread_num(), "Target Data", str, 'B', get_time(),
             args, &tk_list);
  } else {
    // End.

    // Add target task end.
    add_task("OMPT", omp_get_thread_num(), "Target Data", str, 'E', get_time(),
             NULL, &tk_list);
  }
}

/*!
 * Callback to get information about occurrences of a target-data-begin and
 * target-data-end event in that thread.
 * @param kind indicates the kind of target region.
 * @param endpoint indicates whether the callback is signalling the beginning or
 * the end of a scope.
 * @param device_num indicates the id of the device which will execute the
 * target region.
 * @param new_task_data is the generating task.
 * @param target_id is a unique identifier for the associated target region.
 * @param codeptr_ra is used to relate the implementation of an OpenMP region
 * back to its source code.
 */
static void on_ompt_callback_target(ompt_target_t kind,
                                    ompt_scope_endpoint_t endpoint,
                                    int device_num, ompt_data_t *new_task_data,
                                    ompt_id_t target_id,
                                    const void *codeptr_ra) {
  // Concat id with task name.
  char *str = conc_str_int("target_", target_id);

  if (endpoint == ompt_scope_begin) {
    // Begin.
    arg *args = NULL;
    char *str_arg;

    // Add ompt data.
    if (new_task_data->ptr == NULL) {
      new_task_data->ptr = task_data_pointer();
      ((task_data *)new_task_data->ptr)->args = NULL;
    } else
      args = ((task_data *)new_task_data->ptr)->args;

    // Add ompt information.
    str_arg = conc_str_int("", device_num);
    args = add_args(args, "Device num:", str_arg, &tk_list);

    switch (kind) {
    case ompt_target:
      str_arg = "Target.";
      break;
    case ompt_target_enter_data:
      str_arg = "Enter data.";
      break;
    case ompt_target_exit_data:
      str_arg = "Exit data.";
      break;
    case ompt_target_update:
      str_arg = "Update.";
      break;
    default:
      str_arg = "Unknown.";
      break;
    }
    args = add_args(args, "Type of target region:", str_arg, &tk_list);

    ((task_data *)new_task_data->ptr)->args = args;

    // Add target task begin.
    add_task("OMPT", omp_get_thread_num(), "Target Region", str, 'B',
             get_time(), ((task_data *)new_task_data->ptr)->args, &tk_list);
  } else if (new_task_data != NULL) {
    // End.
    if (new_task_data->ptr != NULL) {

      // Add target task end.
      add_task("OMPT", omp_get_thread_num(), "Target Region", str, 'E',
               get_time(), ((task_data *)new_task_data->ptr)->args, &tk_list);
    }
  }
}

// Register callbacks.
#define register_callback_t(name, type)                                        \
  do {                                                                         \
    type f_##name = &on_##name;                                                \
    if (ompt_set_callback(name, (ompt_callback_t)f_##name) == ompt_set_never)  \
      printf("0: Could not register callback '" #name "'\n");                  \
  } while (0)

#define register_callback(name) register_callback_t(name, name##_t)

/*!
 * Initialize tool.
 * @param lookup is a callback to an OpenMP runtime routine that a tool must use
 * to obtain a pointer to each runtime entry point in the OMPT interface.
 * @param initial_device_num.
 * @param data is a pointer to the tool_data field in the
 * ompt_start_tool_result_t structure returned by ompt_start_tool.
 * @return a non-zero value if it succeeds.
 */
int ompt_initialize(ompt_function_lookup_t lookup, int initial_device_num,
                    ompt_data_t *data) {
  ompt_set_callback_t ompt_set_callback =
      (ompt_set_callback_t)lookup("ompt_set_callback");
  ompt_get_thread_data = (ompt_get_thread_data_t)lookup("ompt_get_thread_data");

  // Get callbacks selection.
  FILE *fp;
  char *buffer;
  config_selection s_c;

  DP("------ DEBUG INFORMATION ------\n");

  // Get json file.
  fp = fopen(getenv("OMPTRACING_CONFIG_PATH"), "r");
  // Use default path.
  if (fp == NULL) {
    fp = fopen("config.json", "r");
    if (fp != NULL)
      DP("- Get config settings from config.json.\n");
  } else {
    DP("- Get config settings from OMPTRACING_CONFIG_PATH: %s.\n",
       getenv("OMPTRACING_CONFIG_PATH"));
  }
  // Use default values.
  if (fp == NULL) {
    get_config_default(&s_c);
    DP("- Get default config settings.\n");
  } else {
    fseek(fp, 0, SEEK_END);
    int len = ftell(fp);
    buffer = (char *)malloc(sizeof(char) * len);
    fseek(fp, 0, SEEK_SET);
    fread(buffer, len, 1, fp);
    fclose(fp);

    // Check error.
    if (get_selected_callbacks(buffer, &s_c) == CONFIG_ERROR) {
      printf("Json format incorrect! The tracing will use default config "
             "settings.\n");
      get_config_default(&s_c);
    }
  }

  DP("- Callbacks selected: \n");

  // Register callbacks.
  if (s_c.task == CONFIG_SELECTED) {
    register_callback(ompt_callback_task_create);
    register_callback(ompt_callback_task_schedule);
    register_callback(ompt_callback_dependences);
    DP("    task\n");
  }
  if (s_c.task_create == CONFIG_SELECTED) {
    task_create = CONFIG_SELECTED;
    if (s_c.task == CONFIG_NOT_SELECTED)
      register_callback(ompt_callback_task_create);
    DP("    task_create\n");
  } else
    task_create = CONFIG_NOT_SELECTED;
  if (s_c.thread == CONFIG_SELECTED) {
    register_callback(ompt_callback_thread_begin);
    register_callback(ompt_callback_thread_end);
    DP("    thread\n");
  }
  if (s_c.parallel == CONFIG_SELECTED) {
    register_callback(ompt_callback_parallel_begin);
    register_callback(ompt_callback_parallel_end);
    DP("    parallel\n");
  }
  if (s_c.work == CONFIG_SELECTED) {
    register_callback(ompt_callback_work);
    DP("    work\n");
  }
  if (s_c.implicit_task == CONFIG_SELECTED) {
    register_callback(ompt_callback_implicit_task);
    DP("    implicit_task\n");
  }
  if (s_c.master == CONFIG_SELECTED) {
    register_callback(ompt_callback_master);
    DP("    master\n");
  }
  if (s_c.sync_region == CONFIG_SELECTED || s_c.taskwait == CONFIG_SELECTED) {
    register_callback(ompt_callback_sync_region);

    if (s_c.taskwait == CONFIG_SELECTED) {
      tasks_begin_time = (double *)malloc(s_c.max_tasks * sizeof(double));
      for (int i = 0; i < s_c.max_tasks; i++)
        tasks_begin_time[i] = -1;
      tk_list.taskwait = TASKWAIT_SELECTED;
      dp_list.graph_reduction = GRAPH_REDUCTION_SELECTED;
      DP("    taskwait\n");
    } else
      dp_list.graph_reduction = GRAPH_REDUCTION_NOT_SELECTED;

    DP("    sync_region\n");
  } else {
    tk_list.taskwait = TASKWAIT_NOT_SELECTED;
    dp_list.graph_reduction = GRAPH_REDUCTION_NOT_SELECTED;
  }
  // Register target callbacks.
  // This callback is required for target callbacks to work.
  if (s_c.target == CONFIG_SELECTED || s_c.target_submit == CONFIG_SELECTED ||
      s_c.target_data_op == CONFIG_SELECTED)
    register_callback(ompt_callback_device_initialize);
  if (s_c.target == CONFIG_SELECTED) {
    register_callback(ompt_callback_target);
    DP("    target\n");
  }
  if (s_c.target_submit == CONFIG_SELECTED) {
    register_callback(ompt_callback_target_submit);
    DP("    target_submit\n");
  }
  if (s_c.target_data_op == CONFIG_SELECTED) {
    register_callback(ompt_callback_target_data_op);
    DP("    target_data_op\n");
  }

  DP("- Number max of tasks selected: %d.\n", s_c.max_tasks);

  // Get graph time
  if (s_c.graph_time == CONFIG_SELECTED) {
    dp_list.graph_time = GRAPH_TIME_SELECTED;
    DP("- Graph time selected.\n");
  } else {
    dp_list.graph_time = GRAPH_TIME_NOT_SELECTED;
    DP("- Graph time not selected.\n");
  }

  // Get critical path
  if (s_c.critical_path == CONFIG_SELECTED) {
    dp_list.critical_path = CRITICAL_PATH_SELECTED;
    DP("- Critical path selected.\n");
  } else {
    dp_list.critical_path = CRITICAL_PATH_NOT_SELECTED;
    DP("- Critical path not selected.\n");
  }

  // Get max number of tasks
  dp_list.dependence_size = s_c.max_tasks;

  // Initialize task list.
  initialize_list(&tk_list, &error);
  // Initialize out list
  initialize_task_data_list(&out);
  // Initialize in list
  initialize_task_data_list(&in);
  // Initialize taskwait list.
  initialize_task_data_list(&tkwait_list);

  // Get initial time
  initial_time = get_time();

  // Verify error in json file write.
  if (error == ERROR_NOT_OCCUR) {
    // Initialize writer thread.
    pthread_t writer_thread;
    pthread_create(&writer_thread, NULL, write_tasks, &tk_list);
    add_task("OMPT", -1, "Tracing", "Initialize", 'I', get_time(), NULL,
             &tk_list);
  } else
    printf("Can't write omptracing file.\n");

  // Initialize dependence list.
  error = initialize_dependence(&dp_list);

  // Verify error in graph dependence file write.
  if (error == ERROR_NOT_OCCUR) {
    // Initialize writer thread.
    if (dp_list.graph_reduction == GRAPH_REDUCTION_NOT_SELECTED &&
        dp_list.critical_path == CRITICAL_PATH_NOT_SELECTED) {
      pthread_t writer_thread;
      pthread_create(&writer_thread, NULL, write_dependences, &dp_list);
    }

    if (dp_list.graph_time == GRAPH_TIME_SELECTED) {
      // Initialize time task graph list
      error = initialize_time_list(&tk_time_list);
      tk_time_list.mutex = dp_list.mutex;

      // Verify error in time task graph list
      if (error == ERROR_NOT_OCCUR) {
        // Initialize writer thread.
        pthread_t writer_thread_time;
        pthread_create(&writer_thread_time, NULL, write_time_list,
                       &tk_time_list);
      } else
        printf("Can't write time in graph file.\n");
    }
  } else
    printf("Can't write graph file.\n");

  return 1; // success.
}

/*!
 * Finalize tool.
 * @param data is a pointer to the tool_data field in the
 * ompt_start_tool_result_t structure returned by ompt_start_tool.
 */
void ompt_finalize(ompt_data_t *data) {
  if (error == ERROR_OCCURRED)
    return;

  add_task("OMPT", -1, "Tracing", "Finalize", 'I', get_time(), NULL, &tk_list);

  if (tk_list.taskwait == TASKWAIT_SELECTED) {
    add_taskwait_dependences(&tkwait_list, &dp_list, tasks_begin_time);
  }

  if (dp_list.graph_time == GRAPH_TIME_SELECTED) {
    finalize_time_list(&tk_time_list);
    pthread_t writer_thread_time = tk_time_list.writer;
    pthread_join(writer_thread_time, NULL);
  }

  finalize_dependence(&dp_list);
  if (dp_list.graph_reduction == GRAPH_REDUCTION_NOT_SELECTED) {
    pthread_t writer_thread_dp = dp_list.writer;
    pthread_join(writer_thread_dp, NULL);
  }

  finalize_list(&tk_list);
  pthread_t writer_thread = tk_list.writer;
  pthread_join(writer_thread, NULL);
}

/*!
 * Start tool.
 * @param omp_version is the value of the _OPENMP version macro associated with
 * the OpenMP API implementation.
 * @param runtime_version is a version string that unambiguously identifies the
 * OpenMP implementation.
 * @return a non-NULL pointer to an ompt_start_tool_result_t structure, which
 * contains pointers to tool initialization and finalization callbacks along
 * with a tool data word that an OpenMP implementation must pass by reference to
 * these callbacks, to indicate its intent to use the OMPT interface. Or return
 * NULL if a tool implements ompt_start_tool but has no interest in using the
 * OMPT interface in a particular execution.
 */
ompt_start_tool_result_t *ompt_start_tool(unsigned int omp_version,
                                          const char *runtime_version) {
  static double time = 0;
  time = omp_get_wtime();
  static ompt_start_tool_result_t ompt_start_tool_result = {
      &ompt_initialize, &ompt_finalize, {.ptr = &time}};
  return &ompt_start_tool_result;
}

/*!
 * Tagging begin (indicates when an event begins). Similar to
 * omptracingTagEventBegin, but permits choose the tag_name too.
 * @param tag_name is the name of tagging.
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagBegin(char *tag_name, char *event_name) {
  add_task("OMPT", -1, tag_name, event_name, 'B', get_time(), NULL, &tk_list);
}

/*!
 * Tagging end (indicates when an event ends). Similar to
 * omptracingTaggingEventEnd, but permits choose the tag_name too.
 * @param tag_name is the name of tagging.
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagEnd(char *tag_name, char *event_name) {
  add_task("OMPT", -1, tag_name, event_name, 'E', get_time(), NULL, &tk_list);
}

/*!
 * Tagging event begin (indicates when an event begins).
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagEventBegin(char *event_name) {
  add_task("OMPT", -1, TAG_DEFAULT_NAME, event_name, 'B', get_time(), NULL,
           &tk_list);
}

/*!
 * Tagging pop (indicates when an event ends).
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagEventEnd(char *event_name) {
  add_task("OMPT", -1, TAG_DEFAULT_NAME, event_name, 'E', get_time(), NULL,
           &tk_list);
}
