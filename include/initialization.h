#ifndef _INITIALIZATION_H_
#define _INITIALIZATION_H_

#include "config-sel.h"
#include "graph.h"
#include "tracing.h"

#include <omp-tools.h>
#include <omp.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define NUM_IDS 6

/*!
  \def TAG_DEFAULT_NAME
  The default tag name showed on OmpTracing timeline.
*/
#define TAG_DEFAULT_NAME "Tagging"

/** \struct task_data
    \brief Used to mantain task data information between begin/end regions.
*/
typedef struct task_data {
  double time;          /**< create time of the task. */
  double exec_time;     /**< begin time of the task. */
  uint64_t id[NUM_IDS]; /**< array of each region identifiers. */
  uint64_t parent_id;   /**< parent task identifier. */
  int level;            /**< indicate task level, the height of task tree. */
  arg *args;            /**< ompt task information. */
} task_data;

static task_list tk_list; /**< task list to add callback information. */
static time_task_list
    tk_time_list; /**< task list to add execution time in graph. */
static dependence_list
    dp_list; /**< dependence list to add graph dependence information. */
static task_data_list out, in; /**< task list to add out and in dependencies. */
static task_data_list
    tkwait_list;        /**< task list to add taskwait dependencies. */
static int task_create; /**< indicate if callback task_create was selected, your
                           values are defined by constants SELECTED_CALLBACK and
                           NOT_SELECTED_CALLBACK. */
static double *tasks_begin_time; /**< keep tasks begin time */
static double initial_time = 0;  /**< keep initial time of program */

#endif // _INITIALIZATION_H_
