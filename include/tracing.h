#ifndef _TRACING_H_
#define _TRACING_H_

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/*!
  \def ERROR_OCCURRED
  Indicates when a tracing error occurs.
  \def ERROR_NOT_OCCUR
  Indicates when a tracing error does not occur.
*/
#ifndef ERROR_OCCURRED
#define ERROR_OCCURRED 1
#endif
#ifndef ERROR_NOT_OCCUR
#define ERROR_NOT_OCCUR 0
#endif
/*!
  \def FINISH_WRITE
  Indicates when writing of the trace file is finished.
  \def NOT_FINISH_WRITE
  Indicates when writing of the trace file didn't finish.
*/
#define FINISH_WRITE 1
#define NOT_FINISH_WRITE 0
/*!
  \def FIRST_TASK
  Indicates that the next task to be written is the first.
  \def NOT_FIRST_TASK
  Indicates that the next task to be written is not the first.
*/
#define FIRST_TASK 1
#define NOT_FIRST_TASK 0

/** \struct arg
    \brief Used to mantain ompt information about task data.
*/
typedef struct arg {
  char *label;      /**< information description. */
  void *value;      /**< information value. */
  struct arg *next; /**< next information. */
} arg;

/** \struct task
    \brief Used to mantain task information to write json file.
*/
typedef struct task {
  char *cat;
  int pid;           /**< thread number. */
  char *tid;         /**< task label. */
  double ts;         /**< time of task. */
  char ph;           /**< event type. */
  char *name;        /**< task name. */
  arg *args;         /**< ompt task information. */
  struct task *next; /**< next task. */
} task;

/** \struct task_list
    \brief Used to mantain task list information.
*/
typedef struct task_list {
  task *head;            /**< head list. */
  pthread_t writer;      /**< writer thread. */
  pthread_mutex_t mutex; /**< task list mutex. */
  task *next;            /**< next task. */
  int taskwait; /**< indicates if taskwait are selected or not selected.*/
} task_list;

arg *add_args(arg *args, char *label, void *value, task_list *tk_list);
void *get_arg(arg *args, char *label);
void add_task(char *cat, int pid, char *tid, char *name, char ph, double ts,
              arg *args, task_list *tk_list);
void *write_tasks(void *tasks);
void finalize_list(task_list *tk_list);
void initialize_list(task_list *tk_list, int *error);

#endif // _TRACING_H_
