#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <omp-tools.h>

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/*!
  \def ERROR_OCCURRED
  Indicates when a tracing error occurs.
  \def ERROR_NOT_OCCUR
  Indicates when a tracing error does not occur.
*/
#ifndef ERROR_OCCURRED
#define ERROR_OCCURRED 1
#endif
#ifndef ERROR_NOT_OCCUR
#define ERROR_NOT_OCCUR 0
#endif
/*!
  \def FINISH_WRITE
  Indicates when writing of the trace file is finished.
  \def NOT_FINISH_WRITE
  Indicates when writing of the trace file didn't finish.
*/
#ifndef FINISH_WRITE
#define FINISH_WRITE 1
#endif
#ifndef NOT_FINISH_WRITE
#define NOT_FINISH_WRITE 0
#endif

/*!
  \def GRAPH_REDUCTION_SELECTED
  Indicates when graph-reduction is selected.
  \def GRAPH_REDUCTION_NOT_SELECTED
  Indicates when graph-reduction is not selected.
*/
#define GRAPH_REDUCTION_SELECTED 1
#define GRAPH_REDUCTION_NOT_SELECTED 0

/*!
  \def CRITICAL_PATH_SELECTED
  Indicates when critical path is selected.
  \def CRITICAL_PATH_NOT_SELECTED
  Indicates when critical path is not selected.
*/
#define CRITICAL_PATH_SELECTED 1
#define CRITICAL_PATH_NOT_SELECTED 0

/*!
  \def GRAPH_TIME_SELECTED
  Indicates when graph-time is selected.
  \def GRAPH_TIME_NOT_SELECTED
  Indicates when graph-time is not selected.
*/
#define GRAPH_TIME_SELECTED 1
#define GRAPH_TIME_NOT_SELECTED 0

/*!
  \def TASKWAIT_SELECTED
  Indicates when taskwait is selected.
  \def TASKWAIT_NOT_SELECTED
  Indicates when taskwait is not selected.
*/
#define TASKWAIT_SELECTED 1
#define TASKWAIT_NOT_SELECTED 0

/*!
  \def TASKWAIT_TYPE
  Indicates whether the task is of taskwait type.
  \def NOT_TASKWAIT_TYPE
  Indicates whether the task is not of taskwait type.
*/
#define TASKWAIT_TYPE 1
#define NOT_TASKWAIT_TYPE 0

/*!
  \def ROOT_TASK
  Indicates whether the task is a root task.
  \def NOT_ROOT_TASK
  Indicates whether the task is not a root task.
*/
#define ROOT_TASK 1
#define NOT_ROOT_TASK 0

/*!
  \def CRITICAL_PATH
  Indicates whether the task is part of the critical path.
  \def NOT_CRITICAL_PATH
  Indicates whether the task is not part of the critical path.
*/
#define CRITICAL_PATH 1
#define NOT_CRITICAL_PATH 0

/*!
  \def VISITED
  Indicates whether the task is already visited.
  \def NOT_VISITED
  Indicates whether the task is not visited yet.
*/
#define VISITED 1
#define NOT_VISITED 0

/*!
  \def HAS_PATH
  Indicates whether has path between two tasks.
  \def HAS_NOT_PATH
  Indicates whether has not path between two tasks.
*/
#define HAS_PATH 1
#define HAS_NOT_PATH 0

/** \struct dependence
    \brief Used to mantain dependence information to write graph file.
*/
typedef struct dependence {
  unsigned int id_sink;    /**< id of the sink task. */
  unsigned int id_source;  /**< id of the source task. */
  struct dependence *next; /**< next dependence. */
} dependence;

/** \struct dependence_list
    \brief Used to mantain dependence graph information.
*/
typedef struct dependence_list {
  dependence *head;      /**< head list. */
  pthread_mutex_t mutex; /**< dependence list mutex. */
  pthread_t writer;      /**< writer thread. */
  dependence *next;      /**< next dependence. */
  int dependence_size;   /**< dependence array size. */
  int max_task_id;       /**< max task id. */
  int graph_reduction;   /**< indicates if the graph reduction was selected. */
  int graph_time;        /**< indicates if the graph time was selected. */
  int critical_path;     /**< indicates if the critical path was selected. */
} dependence_list;

/** \struct time_task
    \brief Used to mantain time task information to write graph file.
*/
typedef struct time_task {
  unsigned int id; /**< id of the task. */
  double begin;    /**< begin time of task. */
  double end;      /**< end time of task. */
  int taskwait;    /**< indicates whether the task is of taskwait type. */
  struct time_task *next; /**< next task time. */
} time_task;

/** \struct time_task_list
    \brief Used to mantain task time graph information.
*/
typedef struct time_task_list {
  time_task *head;       /**< head list. */
  pthread_mutex_t mutex; /**< time task list mutex. */
  pthread_t writer;      /**< writer thread. */
  time_task *next;       /**< next time task. */
  int finish_time_list;  /**< Indicates when writing of the time list task is
  finished (FINISH_WRITE) or  didn't finish (NOT_FINISH_WRITE). */
  int finish_thread_time;
} time_task_list;

/** \struct ompt_task_data
    \brief Used to mantain ompt task data information.
*/
typedef struct ompt_task_data {
  void *tk_data;               /**< ompt pointer. */
  uint64_t id;                 /**< omptracing task id. */
  uint64_t value;              /**< ompt task id. */
  struct ompt_task_data *next; /**< next ompt task data. */
} ompt_task_data;

/** \struct task_data_list
    \brief Used to mantain ompt task data list information.
*/
typedef struct task_data_list {
  uint64_t last_id;      /**< last omptracing task id added. */
  ompt_task_data *head;  /**< head list. */
  pthread_mutex_t mutex; /**< ompt task data list mutex. */
  ompt_task_data *next;  /**< next task. */
} task_data_list;

/** \struct task_array_data
    \brief Used to mantain task information to write task graph with critical
   path.
*/
typedef struct task_array_data {
  double elapsed_time; /**< elapsed_time time of task. */
  double path_time;    /**< elapsed_time time of child longest path. */
  int root;            /**< indicates whether the task is a root task. */
  int critical_path;   /**< indicates whether the task is part of critical path.
                        */
  int visited;         /**< indicates if the task is already visited. */
  unsigned int next_child_longest_path; /**< next child task id that is part of
                                           longest path. */
} task_array_data;

void add_dependence(unsigned int id_sink, unsigned int id_source,
                    dependence_list *dp_list);
void *write_dependences(void *dependences);
void finalize_dependence(dependence_list *dp_list);
int initialize_dependence(dependence_list *dp_list);
void add_time_task(unsigned int id, double begin, double end,
                   time_task_list *tk_time_list, int taskwait);
void add_elapsed_time(unsigned int id, double elapsed_time,
                      dependence_list *dp_list);
void *write_time_list(void *tasks);
void finalize_time_list(time_task_list *tk_time_list);
int initialize_time_list(time_task_list *tk_time_list);
void add_task_data(ompt_data_t *task, uint64_t id, uint64_t value,
                   task_data_list *tk_list);
int check_path(uint64_t sink, uint64_t src, dependence_list *dp_list);
int search_in_dep(ompt_data_t *task, uint64_t id, uint64_t value,
                  task_data_list *tk_list, dependence_list *dp_list);
int search_out_dep(ompt_data_t *task, uint64_t id, uint64_t value,
                   task_data_list *tk_list, dependence_list *dp_list);
void initialize_task_data_list(task_data_list *tk_list);
void add_taskwait_dependences(task_data_list *tk_list, dependence_list *dp_list,
                              double *begin_time_list);
int taskwait_exists(uint64_t id, task_data_list *tk_list);

#endif // _GRAPH_H_
