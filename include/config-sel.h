#ifndef _CALLBACKS_SEL_H_
#define _CALLBACKS_SEL_H_

#include <json.h>
#include <stdio.h>
#include <string.h>

/*!
  \def CONFIG_SELECTED
  Indicates when a configuration is selected.
  \def CONFIG_NOT_SELECTED
  Indicates when a configuration is not selected.
*/
#define CONFIG_SELECTED 1
#define CONFIG_NOT_SELECTED 0

/*!
  \def CONFIG_ERROR
  Indicates when a config error occurs.
  \def CONFIG_NOT_ERROR
  Indicates when a config error does not occur.
*/
#define CONFIG_ERROR 1
#define CONFIG_NOT_ERROR 0

/*!
  \def MAX_TASKS_DEFAULT
  The maximum number of tasks that will be allocated on the task-graph if it not
  defined by the config selector.
*/
#define MAX_TASKS_DEFAULT 2048

/** \struct config_selection
    \brief Used to choose which callbacks are selected and to set graph config.
*/
typedef struct config_selection {
  int task, task_create, thread, parallel, work, implicit_task, master,
      sync_region, target, target_submit, target_data_op,
      taskwait;                             /**< callback types. */
  int max_tasks, graph_time, critical_path; /**< graph config. */
} config_selection;

void get_config_default(config_selection *config);
int get_selected_callbacks(char *buffer, config_selection *config);

#endif // _CALLBACKS_SEL_H_
