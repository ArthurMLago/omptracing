cmake_minimum_required (VERSION 3.10)
project(omptracing)

# add a target to generate API documentation with Doxygen
find_package(Doxygen)
option(BUILD_DOCUMENTATION "Create and install the HTML based API documentation (requires Doxygen)" ${DOXYGEN_FOUND})

if(BUILD_DOCUMENTATION)
  if(NOT DOXYGEN_FOUND)
    message(FATAL_ERROR "Doxygen is needed to build the documentation.")
  endif()

  set(doxyfile_in ${CMAKE_CURRENT_SOURCE_DIR}/doxygen.in)
  set(doxyfile ${CMAKE_CURRENT_BINARY_DIR}/doxygen.cfg)

  configure_file(${doxyfile_in} ${doxyfile} @ONLY)

  add_custom_target(doc
    COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating API documentation with Doxygen"
    VERBATIM)
endif()

find_package(PkgConfig REQUIRED)
pkg_search_module(JSON_C REQUIRED json-c)

find_package(OpenMP REQUIRED)

if (OPENMP_FOUND)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")

  if (OPENMP_TARGETS)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fopenmp-targets=${OPENMP_TARGETS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp-targets=${OPENMP_TARGETS}")
    add_definitions("-DOPENMP_TARGETS")
  endif()

endif()

option(OMPTRACING_DEBUG "Allow to print debug message" TRUE)
if (OMPTRACING_DEBUG)
    add_definitions("-DOMPTRACING_DEBUG")
endif()

file(GLOB_RECURSE
  CHECK_SOURCE_FILES
  src/*.c src/*.h
  include/*.c include/*.h
  example/*.c
)
include("cmake/ClangFormat.cmake")

include_directories(
  include
  ${JSON_C_INCLUDE_DIRS}
)

add_library(omptracing SHARED
    src/initialization.c
    src/config-sel.c
    src/graph.c
    src/tracing.c
)

target_link_libraries(omptracing ${JSON_C_LIBRARIES})

set_target_properties(omptracing
  PROPERTIES PUBLIC_HEADER include/omptracing.h
)

# Destination of the installed config files (relative path):
set(CMAKE_CONFIG_DEST "cmake")

include(CMakePackageConfigHelpers)

# Generate in-tree config file
set(omptracing_INCLUDE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/include")

configure_package_config_file(
  "omptracing-config.cmake.in"
  "${CMAKE_BINARY_DIR}/omptracing-config.cmake"
  INSTALL_DESTINATION "${CMAKE_CONFIG_DEST}"
  PATH_VARS omptracing_INCLUDE_DIRS
)

# Generate config file for installation
set(omptracing_INCLUDE_DIRS "include")

# Write 'export' folder not to collide with previous config file
configure_package_config_file (
  "omptracing-config.cmake.in"
  "${CMAKE_BINARY_DIR}/export/omptracing-config.cmake"
  INSTALL_DESTINATION "${CMAKE_CONFIG_DEST}"
  PATH_VARS omptracing_INCLUDE_DIRS
)

export(
  TARGETS omptracing
  FILE "${CMAKE_BINARY_DIR}/omptracingTargets.cmake"
)

install(
  TARGETS omptracing
  EXPORT omptracingTargets
  LIBRARY DESTINATION lib
  PUBLIC_HEADER DESTINATION include
)

install(
  FILES "${CMAKE_BINARY_DIR}/export/omptracing-config.cmake"
  DESTINATION ${CMAKE_CONFIG_DEST}
)

install(EXPORT omptracingTargets DESTINATION ${CMAKE_CONFIG_DEST})
