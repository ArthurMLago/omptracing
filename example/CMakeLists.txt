cmake_minimum_required (VERSION 3.10)
project(hello)

# FIXME: Only required to fix CI job
list(APPEND CMAKE_PREFIX_PATH ${PROJECT_SOURCE_DIR}/../build-lib/)

find_package(OpenMP REQUIRED)

find_package(omptracing CONFIG REQUIRED)

include_directories(
  ${omptracing_INCLUDE_DIRS}
)

if (OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")

    if (OPENMP_TARGETS)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fopenmp-targets=${OPENMP_TARGETS}")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp-targets=${OPENMP_TARGETS}")
        add_definitions("-DOPENMP_TARGETS")
    endif()

endif()

add_executable(hello hello.c)
add_executable(hello-target hello-target.c)
add_executable(tag-example tag-example.c)

target_link_libraries(tag-example omptracing)
