#include "omptracing.h"
#include <omp.h>
#include <stdio.h>

int main() {
  int i, v[10], t[10];

  // Tag Name: Default
  // Event Name: first_computation
  omptracingTagEventBegin("first_computation");
#pragma omp parallel for num_threads(2)
  for (i = 0; i < 1000000; i++)
    v[i % 10] += i;
  omptracingTagEventEnd("first_computation");

  // Tag Name: Example
  // Event Name: second_computation
  omptracingTagBegin("Example", "second_computation");
#pragma omp parallel for num_threads(2)
  for (i = 0; i < 1000000; i++)
    t[i % 10] += i * i;
  omptracingTagEnd("Example", "second_computation");

  return 0;
}
