#include <omp.h>
#include <stdio.h>

int main() {
// Test target.
#pragma omp target
  {
    if (!omp_is_initial_device())
      printf("Hello World from accelerator\n");
    else
      printf("Hello World from host\n");
  }

  int N = 100;
  double *array = (double *)malloc(sizeof(double) * N);
// Target data region
#pragma omp target data map(from : array [0:N])
  {
// The first target region
#pragma omp target map(tofrom : array [0:N])
    {
      for (int i = 0; i < N; i++)
        array[i] = (double)i / N;
    }
// The second target region
#pragma omp target map(tofrom : array [0:N])
    {
      for (int i = 0; i < N; i++)
        array[i] = 1.0 / array[i];
    }
  }

  return 0;
}
